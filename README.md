# ansible-steamcmd

This project is incomplete. Use at your own risk.

Currently, this playbook can only be used to install TF2. The intent of this playbook is obviously to install ANY steamcmd game, but it is still in very early stages of development.

Supported distributions:
- Debian 7+
- Red Hat / CentOS 6+
- Ubuntu 16.04 (requires manual installation of python-simplejson)

Fedora support has been dropped because it makes the conditional logic in redhat.yml too complicated.
